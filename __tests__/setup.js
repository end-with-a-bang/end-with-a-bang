describe("jest setup", () => {
	it("should work with simple assertion", () => {
		expect(true).toEqual(true);
	});
	it("should work with async/await assertion", async () => {
		expect.assertions(1);
		// eslint-disable-next-line
		const promise = new Promise(resolve => {
			setTimeout(() => {
				resolve(true);
			}, 2000);
		});
		const result = await promise;
		expect(result).toEqual(true);
	});
});
