describe("test setup", () => {
	it.each`
		actual  | expected
		${1}    | ${1}
		${2}    | ${2}
		${true} | ${true}
	`("should have table working for $actual", ({ actual, expected }) => {
		expect(actual).toEqual(expected);
	});
});
