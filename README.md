# End With A BANG!

## Architecture

## Well-Architected practice

1. Use GitLab CI to automate testing, build and deployment.
2. Use AWS Elastic Beanstalk, which in turn uses [AWS Cloudformation](https://aws.amazon.com/cloudformation/) to manage the resources into one stack. the AWS Elastic Beanstalk is stored under the `.ebextensions` folder, as part of the Infrastructure as code practice.
3. Using different Elastic Beanstalk Environment, the staging environment is automatically shut down, via [AWS Cloud Watch alarm](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-cw-alarm.html) (embedded in this [ebextension file](.ebextensions/sns.config#L28)) and GitLab CI pipeline via [API](https://docs.gitlab.com/ee/ci/triggers/).

## AWS Resources

###### AWS Elastic Beanstalk

This application is bootstrapped/built on using Elastic Beanstalk. With different configuration to orchestrate different AWS resources to build a web application.
The platform chosen for this project is [Single Container Docker](https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.docker).

##### Setup

_S3_

1. Set up a S3 bucket that will store the source bundle.

```sh
$ aws mb s3://$BUCKET_NAME --region $AWS_REGION
```

_Elastic Beanstalk_

1. Create the Elastic Beanstalk application

    An application is required.

```sh
$ aws elasticbeanstalk create-application --application-name end-with-a-bang --description 'end with a bang application' --resource-lifecycle-config 'file://infrastructure/elasticbeanstalk/application/application-lifecycle.json' --debug
```

2. Create application configuration

    Under the created application, create a new env, with the config under `.ebextensions`

```sh
$ aws elasticbeanstalk create-configuration-template --application-name end-with-a-bang --template-name 'alb-ec2-key-pair' --solution-stack-name '64bit Amazon Linux 2018.03 v2.10.0 running Docker 17.12.1-ce'  --description 'single docker config with ALB'
```

3. Create a new environment for deploying application

```sh
$ aws elasticbeanstalk create-environment --application-name end-with-a-bang --environment-name staging --description 'staging env for end with a bang, correspond to develop branch' --cname-prefix staging-end-with-a-bang --template-name alb-ec2-key-pair --tags Key=env,Value=staging
```

4. Terminate environment

    To terminate an environment (to save money)

```sh
$ aws elasticbeanstalk terminate-environment --application-name end-with-a-bang --environment-name staging
```

## CI/CD

This repo uses GitLab CI/CD to automatically deploy to AWS

###### CI/CD Env Env

The follow env var should exists

| Environment related entity        | GitLab CI var          | Note                                    |
| --------------------------------- | ---------------------- | --------------------------------------- |
| S3 Bucket name                    | \$S3_BUCKET            | S3 Bucket that store the source bundle  |
| ElasticBeanstalk application name | \$EB_APP_NAME          |                                         |
| Prod env id                       | \$EB_PROD_ENV_ID       |                                         |
| Prod env name                     | \$EB_PROD_ENV_NAME     |                                         |
| Staging env id                    | \$EB_STAGING_ENV_ID    |                                         |
| Staging env name                  | \$EB_STAGING_ENV_NAME  |                                         |
| Docker Hub Password               | \$DOCKER_HUB_PASSWORD  |                                         |
| Docker Hub Username               | \$DOCKER_REGISTRY_USER |                                         |
| Gitlab CI Token                   | \$GITLAB_CI_TOKEN      | This token is used to activate a CI job |

###### CI/CD Strategy

1. Build the Docker image in GitLab CI
2. Push the Docker image to Docker Hub in GitLab CI
3. On the fly replace the tag in the `Dockerrun.aws.json` with the tag
4. Proceed to update the environment

###### Deployment Strategy

Deployment to Staging environment is automatic when a `develop` branch is updated. In the CI process, a docker image tagged with a shorten commit hash is built and pushed to [Docker Hub](https://hub.docker.com). `Dockerrun.aws.json` is modified to reference this docker image, a new [application version](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/applications-versions.html) is created and pubished. This version is then deployed.
Deployment to Production environment is largely the same, but the CI pipeline is triggered by a new tag, in regex expression of `/^v\d+\.\d+\.\d+$/`; instead of the shorten commit hash, the tagged version is used.
