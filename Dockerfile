FROM node:8.14.0-jessie

WORKDIR /app

# Get the logging folder setup
RUN mkdir .logs
RUN touch /app/.logs/access.log
# RUN chmod 666 .logs/access.log

COPY next.config.js package-lock.json package.json /app/
RUN npm install --production

COPY contexts/ contexts/
COPY constants/ constants/
COPY utils/ utils/
COPY pages/ pages/
COPY static/ static/
COPY components/ components/
COPY server/ server/

RUN npm run build

EXPOSE 3000

CMD ["npm", "start"]
