const axios = require("axios");

// eslint-disable-next-line
exports.handler = async (event, context) => {
	const GITLAB_PROJECT_ID = process.env.GITLAB_PROJECT_ID;
	const GITLAB_CI_TOKEN = process.env.GITLAB_CI_TOKEN;
	const REF = process.env.REF || "develop";
	const endpoint = `https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/ref/${REF}/trigger/pipeline?token=${GITLAB_CI_TOKEN}&variables[SHUTDOWN_RESOURCES]=true&variables[SHUTDOWN_STAGING_RESOURCES]=true`;
	await axios.post(endpoint, {}).catch(() => {});
};
