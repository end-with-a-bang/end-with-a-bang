const SWPrecacheWebpackPlugin = require("sw-precache-webpack-plugin");

module.exports = {
	onDemandEntries: {
		// period (in ms) where the server will keep pages in the buffer
		maxInactiveAge: 25 * 1000,
		// number of pages that should be kept simultaneously without being disposed
		pagesBufferLength: 2,
		websocketPort: 3007
	},
	serverRuntimeConfig: {
		// Will only be available on the server side
		mySecret: "secret",
		apiURL: process.env.API_URL,
		webhookToken: process.env.WEBHOOK_TOKEN
	},
	publicRuntimeConfig: {
		// Will be available on both server and client
		staticFolder: "/static",
		shortSHA: process.env.SHORT_SHA
	},
	webpack: config => {
		config.plugins.push(
			new SWPrecacheWebpackPlugin({
				verbose: true,
				staticFileGlobsIgnorePatterns: [/\.next\//],
				runtimeCaching: [
					{
						handler: "fastest",
						urlPattern: /^https?.*/,
						options: {
							networkTimeoutSeconds: 2
						}
					}
				]
			})
		);

		return config;
	}
};
