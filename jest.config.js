module.exports = {
	verbose: true,
	collectCoverage: true,
	collectCoverageFrom: [
		"**/*.{js,jsx}",
		"!**/node_modules/**",
		"!<rootDir>/*.config.js",
		"!<rootDir>/jest.setup.js"
	],
	coveragePathIgnorePatterns: [
		"<rootDir>/build/",
		"<rootDir>/node_modules/",
		"<rootDir>/.next/"
	],
	projects: [
		{
			displayName: "test"
		}
	]
	// coverageThreshold: {
	// 	global: {
	// 		branches: 80,
	// 		functions: 80,
	// 		lines: 80,
	// 		statements: -10
	// 	}
	// }
};
