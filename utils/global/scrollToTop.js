export const scrollToTop = (step = 50) => {
	if (typeof step !== "number") {
		step = 50;
	}
	try {
		const scrollStep = -window.scrollY / step;
		let scrollInterval = setInterval(function() {
			if (window.scrollY != 0) {
				window.scrollBy(0, scrollStep);
			} else clearInterval(scrollInterval);
		}, 15);
	} catch {
		// nothing
	}
};
