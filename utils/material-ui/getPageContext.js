// https://raw.githubusercontent.com/mui-org/material-ui/master/examples/nextjs/src/getPageContext.js
import { SheetsRegistry } from "jss";
import {
	createMuiTheme,
	createGenerateClassName
} from "@material-ui/core/styles";
import orange from "@material-ui/core/colors/orange";
import deepPurple from "@material-ui/core/colors/deepPurple";

// A theme with custom primary and secondary color.
// It's optional.
const theme = createMuiTheme({
	palette: {
		primary: orange,
		secondary: deepPurple
	},
	typography: {
		useNextVariants: true
	}
});

function createPageContext() {
	return {
		theme,
		// This is needed in order to deduplicate the injection of CSS in the page.
		sheetsManager: new Map(), // eslint-disable-line
		// This is needed in order to inject the critical CSS.
		sheetsRegistry: new SheetsRegistry(),
		// The standard class name generator.
		generateClassName: createGenerateClassName()
	};
}

let pageContext;

export default function getPageContext() {
	// Make sure to create a new context for every server-side request so that data
	// isn't shared between connections (which would be bad).
	if (!process.browser) {
		return createPageContext();
	}

	// Reuse context on the client-side.
	if (!pageContext) {
		pageContext = createPageContext();
	}

	return pageContext;
}
