# Rules for processing tweets

#### Rules and Priority

There are multiple rules for processing a tweet, and based on these rules and its priority number, the header and misc info are extracted

1. `getPunctuation`

    - remove links and hashtag at the end of the tweets, but get the word that is attached to !, ?

2. `getFirstQuote`
    - get the entire quote when a tweet is started with a quotation mark (unicode character \u201c)
