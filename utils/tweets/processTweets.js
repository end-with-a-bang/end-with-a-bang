import {
	lastBangWithURLHashtag,
	startWithQuote,
	lastCapWord,
	notSpaceWordDigitAndUsefulPunc,
	endHashLink
} from "../../constants/regex";

export const getPunctuation = tweetText => {
	if (!lastBangWithURLHashtag.test(tweetText)) {
		return null;
	}
	const captureGroup = tweetText.match(lastBangWithURLHashtag)[1];
	return captureGroup.replace(notSpaceWordDigitAndUsefulPunc, "");
};

export const getFirstQuote = tweetText => {
	if (!startWithQuote.test(tweetText)) {
		return null;
	}
	const captureGroup = tweetText.match(startWithQuote)[0];
	return captureGroup;
};

export const getLastCapWord = tweetText => {
	if (!lastCapWord.test(tweetText.replace(endHashLink, ""))) {
		return null;
	}
	const captureGroup = tweetText
		.replace(endHashLink, "")
		.match(lastCapWord)[1];
	return captureGroup.replace(notSpaceWordDigitAndUsefulPunc, "").trim(); // captureGroup could have preceding space
};

export const getLastHashAndURL = tweetText => {
	if (!endHashLink.test(tweetText)) {
		return null;
	}
	const captureGroup = tweetText.match(endHashLink)[0];
	return captureGroup.split(/\s+/).map(s => s.trim()); // captureGroup could have preceding space
};

export const getLastHash = tweetText => {
	const hashAndURL = getLastHashAndURL(tweetText);
	return !hashAndURL ? [] : hashAndURL.filter(s => s.startsWith("#"));
};

export const getLastURL = tweetText => {
	const hashAndURL = getLastHashAndURL(tweetText);
	return !hashAndURL ? [] : hashAndURL.filter(s => s.startsWith("http"));
};

export const isRetweet = tweetObj => tweetObj.is_retweet;

export const processTweet = tweetText => {
	// TODO: ADD PRIORITY FOR DIFFERENT RULES
	const fallback = tweetText.substring(0, 18) + "...";
	return {
		fallback,
		punctuation: getPunctuation(tweetText),
		firstQuote: getFirstQuote(tweetText),
		// lastCapWord: getLastCapWord(tweetText),
		lastHash: getLastHash(tweetText),
		lastURL: getLastURL(tweetText)
	};
};

export default processTweet;
