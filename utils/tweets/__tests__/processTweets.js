import {
	getPunctuation,
	getFirstQuote,
	getLastCapWord,
	getLastHash,
	getLastURL,
	isRetweet
} from "../processTweets";
import tweets from "../../../static/tweets.json";

expect.extend({
	toBeTweetId(tweetId) {
		const pass = tweets.find(t => t.id_str === tweetId);
		if (pass) {
			return {
				message: () => `expected ${tweetId} not exists in the tweets`,
				pass: true
			};
		} else {
			return {
				message: () => `expected ${tweetId} to exists in the tweets`,
				pass: false
			};
		}
	}
});

describe("getPunctuation", () => {
	test("should exists", () => {
		expect(getPunctuation).toBeDefined();
	});

	test.each`
		tweet_id                | expected
		${"946519720450252800"} | ${"AGAIN!"}
		${"947810806430826496"} | ${"CHANGE!"}
		${"947236393184628741"} | ${"COME!"}
		${"947235015343202304"} | ${"fiction!"}
		${"946731576687235072"} | ${"cost!"}
		${"944192071535153152"} | ${"country!"}
		${"941291666564141057"} | ${"high!"}
		${"939152197090148352"} | ${"cheating!"}
		${"937142713211813889"} | ${"justice!"}
		${"937007006526959618"} | ${"hide!"}
		${"933282274937733126"} | ${"fool!"}
		${"932433668068528131"} | ${"public!"}
		${"930747213659197441"} | ${"come!"}
		${"930305984077156352"} | ${"friends!"}
		${"928770248370728960"} | ${"Liyuan!"}
		${"928074747316928513"} | ${"before!"}
		${"927847471321141249"} | ${"jobs!"}
		${"923552633633337344"} | ${"crime!"}
		${"923277142271684609"} | ${"interesting!"}
		${"922830229525225477"} | ${"forward!"}
	`(
		"should return endbang when a tweet that last character is ! (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getPunctuation(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);

	test.each`
		tweet_id                | expected
		${"945126026824298497"} | ${"CHRISTMAS!!!!!"}
		${"820707210565132288"} | ${"CHANGE!!!!"}
	`(
		"should return multiple endbang when a tweet final characters are multiple ! (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getPunctuation(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);

	test.each`
		tweet_id                | expected
		${"947461470924820480"} | ${"more?"}
		${"944666448185692166"} | ${"go?!!!"}
		${"944665687292817415"} | ${"investigation?"}
		${"944222157218942978"} | ${"news?"}
		${"939480342779580416"} | ${"incompetence?"}
		${"937141061343956992"} | ${"standard?"}
		${"935701139198181376"} | ${"corrupt?"}
		${"931360196554821632"} | ${"tape?"}
	`(
		"should return multiple question marks when a tweet final characters are multiple ? (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getPunctuation(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);

	test.each`
		tweet_id                | expected
		${"947461470924820480"} | ${"more?"}
		${"944666448185692166"} | ${"go?!!!"}
		${"944665687292817415"} | ${"investigation?"}
		${"944222157218942978"} | ${"news?"}
		${"939480342779580416"} | ${"incompetence?"}
		${"937141061343956992"} | ${"standard?"}
		${"935701139198181376"} | ${"corrupt?"}
		${"931360196554821632"} | ${"tape?"}
	`(
		"should return multiple question marks when a tweet final characters are multiple ? (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getPunctuation(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);

	test.each`
		tweet_id                | expected
		${"942717030091943936"} | ${"well!"}
		${"942123433873281024"} | ${"JOBS!"}
		${"941868639199916037"} | ${"you!"}
		${"941820213518938112"} | ${"highs!"}
		${"941723524921724928"} | ${"CONGRATULATIONS!"}
		${"941406242785955841"} | ${"AGAIN!"}
		${"947544600918372353"} | ${"Year!!"}
		${"947458942719979520"} | ${"beginning!"}
		${"946519720450252800"} | ${"AGAIN!"}
		${"947181212468203520"} | ${"watching!"}
		${"942857031693799425"} | ${"country!"}
		${"942717030091943936"} | ${"well!"}
		${"941723524921724928"} | ${"CONGRATULATIONS!"}
		${"939680422493073408"} | ${"aliens!"}
		${"939299624875118592"} | ${"minutes!"}
		${"936315571708289026"} | ${"AGAIN!"}
		${"936014271858831361"} | ${"COUNTRY!"}
		${"935712632409030656"} | ${"Ivanka!"}
	`(
		"should return endbang even there are links and/or hashtag after the endbang (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getPunctuation(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);

	test.each`
		tweet_id                | expected
		${"945437479863291904"} | ${"anticipated!"}
		${"935841679977910272"} | ${"policies!"}
		${"933918671503753216"} | ${"ratings!"}
		${"925336826717593600"} | ${"corruption!"}
		${"904177758489776129"} | ${"Proclamation!"}
		${"850785347038576640"} | ${"top!"}
	`(
		"should return endbang even when there are close paren preceeding endbang (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getPunctuation(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);

	test.each`
		tweet_id                | expected
		${"946725057384108033"} | ${"SA\u2026"}
		${"935494673744965637"} | ${"partnershi\u2026"}
		${"933291745722351617"} | ${"sho\u2026"}
	`(
		"should return horizontal ellipsis when ends with unicdoe \u2026 (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getPunctuation(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);

	test.each`
		tweet_id                | expected
		${"919554724512387072"} | ${"fast...."}
		${"917921548677328896"} | ${"be....."}
		${"916620603741868032"} | ${"campaign...."}
		${"908276308265795585"} | ${"Really!....."}
		${"906135414498631680"} | ${"will...."}
		${"881271748280365056"} | ${"won...."}
		${"868807754231820291"} | ${"names...."}
	`(
		"should return 4+ periods when ends with 4+ periods (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getPunctuation(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);
});

describe("getFirstQuote", () => {
	test("should exists", () => {
		expect(getFirstQuote).toBeDefined();
	});

	test.each`
		tweet_id                | expected
		${"944210183089254400"} | ${"\u201cThe President has accomplished some absolutely historic things during this past year.\u201d"}
		${"946201376652169220"} | ${"\u201cOn 1/20 - the day Trump was inaugurated - an estimated 35,000 ISIS fighters held approx 17,500 square miles of territory in both Iraq and Syria. As of 12/21, the U.S. military est the remaining 1,000 or so fighters occupy roughly 1,900 square miles..\u201d"}
		${"941417725833998340"} | ${"\u201cManufacturing Optimism Rose to Another All-Time High in the Latest @ShopFloorNAM Outlook Survey\u201d"}
		${"935545697360891904"} | ${"\u201cStatement from President Donald J. Trump on #GivingTuesday\u201d"}
		${"925440868575797252"} | ${"\u201cHome Prices Reach New All-Time Highs in August\u201d"}
		${"924096747122634752"} | ${"\u201cWHAT HAPPENED\u201d"}
		${"922903426434064384"} | ${"\u201cPresident Donald J. Trump Proclaims October 24, 2017, as United Nations Day\u201d"}
		${"946201376652169220"} | ${"\u201cOn 1/20 - the day Trump was inaugurated - an estimated 35,000 ISIS fighters held approx 17,500 square miles of territory in both Iraq and Syria. As of 12/21, the U.S. military est the remaining 1,000 or so fighters occupy roughly 1,900 square miles..\u201d"}
	`(
		"should return the complete quote (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getFirstQuote(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);

	test.each`
		tweet_id                | expected
		${"946725057384108033"} | ${"SA\u2026"}
		${"935494673744965637"} | ${"partnershi\u2026"}
		${"933291745722351617"} | ${"sho\u2026"}
	`(
		"should return horizontal ellipsis when ends with unicdoe \u2026 (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getPunctuation(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);

	test.each`
		tweet_id                | expected
		${"919554724512387072"} | ${"fast...."}
		${"917921548677328896"} | ${"be....."}
		${"916620603741868032"} | ${"campaign...."}
		${"908276308265795585"} | ${"Really!....."}
		${"906135414498631680"} | ${"will...."}
		${"881271748280365056"} | ${"won...."}
		${"868807754231820291"} | ${"names...."}
	`(
		"should return 4+ periods when ends with 4+ periods (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getPunctuation(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);
});

describe("getLastCapWord", () => {
	test("should exists", () => {
		expect(getLastCapWord).toBeDefined();
		expect(getLastCapWord).toEqual(expect.any(Function));
	});

	test.each`
		tweet_id                | expected
		${"947810806430826496"} | ${"TIME FOR CHANGE!"}
		${"947544600918372353"} | ${"Happy New Year!!"}
		${"947236393184628741"} | ${"MUCH MORE TO COME!"}
		${"946728546633953285"} | ${"MUCH MORE!"}
		${"946519720450252800"} | ${"MAKING AMERICA GREAT AGAIN!"}
		${"944908467499884544"} | ${"President TRUMP!!"}
		${"944700332881440769"} | ${"Fake News Media!"}
		${"943908041803657216"} | ${"MAKING AMERICA GREAT AGAIN!"}
		${"943598090069536768"} | ${"WE ARE MAKING AMERICA GREAT AGAIN!"}
		${"943489378462130176"} | ${"Jobs, Jobs, Jobs!"}
		${"942854056162332677"} | ${"MAKE AMERICA GREAT AGAIN!"}
		${"942851788461563906"} | ${"AMERICAN PEOPLE!"}
		${"940554567414091776"} | ${"FAKE NEWS!"}
		${"939616077356642304"} | ${"FAKE NEWS WaPo!"}
		${"939521466634326016"} | ${"Civil Rights Museum!"}
		${"939485131693322240"} | ${"CNN, THE LEAST TRUSTED NAME IN NEWS!"}
	`(
		"should return last phrases with words that start with a cap letter (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getLastCapWord(tweets.find(t => t.id_str === tweet_id)["text"])
			).toBe(expected);
		}
	);
});

describe("getLastHash", () => {
	test("should exists", () => {
		expect(getLastHash).toBeDefined();
		expect(getLastHash).toEqual(expect.any(Function));
	});

	test.each`
		tweet_id                | expected
		${"947536951464333318"} | ${["#LESM"]}
		${"928861178113040385"} | ${["#APEC2017"]}
		${"925818338172915712"} | ${["#NYCStrong"]}
		${"923244308848881665"} | ${["#Dobbs"]}
		${"946460939578167296"} | ${["#NoKo"]}
		${"944659788826324992"} | ${["#JournalismIsDead"]}
		${"941820213518938112"} | ${["#MAGA"]}
		${"941756992070307845"} | ${["#LESM"]}
		${"938755723902713861"} | ${["#MakeAmericaGreatAgain"]}
		${"938469354089385985"} | ${["#Periscope"]}
		${"936351772603568130"} | ${["#DOW24K", "#MAGA"]}
		${"928969836897583104"} | ${["#USMC242", "#SemperFi"]}
	`(
		"should return last phrases with words that start with a cap letter (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getLastHash(tweets.find(t => t.id_str === tweet_id)["text"])
			).toEqual(expected);
		}
	);
});

describe("getLastURL", () => {
	test("should exists", () => {
		expect(getLastURL).toBeDefined();
		expect(getLastURL).toEqual(expect.any(Function));
	});

	test.each`
		tweet_id                | expected
		${"936646898408218626"} | ${["https://t.co/AMqW8mWiak", "https://t.co/XGp8EelNzz"]}
		${"936393574433939456"} | ${["https://t.co/cTvdlUkfHV"]}
		${"936315571708289026"} | ${["https://t.co/PIv9OAVZcf", "https://t.co/6egRvuwj1l"]}
		${"935940131311190021"} | ${["https://t.co/jR1DEUnm2h", "https://t.co/XF9sRwdV8u"]}
		${"946460939578167296"} | ${["https://t.co/LQl7tGhMdO"]}
	`(
		"should return last phrases with words that start with a cap letter (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(
				getLastURL(tweets.find(t => t.id_str === tweet_id)["text"])
			).toEqual(expected);
		}
	);
});

describe("isRetweet", () => {
	test("should exists", () => {
		expect(isRetweet).toBeDefined();
	});

	test.each`
		tweet_id                | expected
		${"921723302481145857"} | ${true}
		${"919314277177192448"} | ${true}
		${"919157838106300416"} | ${true}
		${"915371898774618112"} | ${true}
		${"914255264282480640"} | ${false}
		${"857670562478518273"} | ${false}
		${"829496507841789952"} | ${false}
	`(
		"should return the true it is a retweet (tweet_id $tweet_id)",
		({ tweet_id, expected }) => {
			expect(tweet_id).toBeTweetId();
			expect(isRetweet(tweets.find(t => t.id_str === tweet_id))).toBe(
				expected
			);
		}
	);
});
