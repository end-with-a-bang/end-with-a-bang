import fetch from "isomorphic-fetch";
import { TWEET_PER_PAGE } from "../../constants/tweets";

import getConfig from "next/config";

const getApiURL = () => {
	const nextConfig = getConfig();

	const apiURL =
		nextConfig &&
		nextConfig.serverRuntimeConfig &&
		nextConfig.serverRuntimeConfig.apiURL;
	return apiURL;
};

export const getTweets = (opts = {}) => {
	const { count = TWEET_PER_PAGE, afterId } = opts;
	let qs = "";
	if (afterId) {
		qs += "afterId=" + afterId + "&";
	}
	qs += "count=" + count;
	return fetch(getApiURL() + "?" + qs)
		.then(r => r.json())
		.then(r => {
			return r.tweets;
		});
};

export default { getTweets };
