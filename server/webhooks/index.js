import express from "express";
import getConfig from "next/config";

import { UPDATE_FAKE_ID } from "../../constants/event-constants";

const getWebhookToken = () => {
	const nextConfig = getConfig();
	const webhookToken =
		nextConfig &&
		nextConfig.serverRuntimeConfig &&
		nextConfig.serverRuntimeConfig.webhookToken;
	return webhookToken;
};
/**
 * A middleware to check incoming HTTP request for webhook token in header
 * @param {*} req express req
 * @param {*} res express res
 * @param {*} next express next
 */
const checkWebhookToken = (req, res, next) => {
	const webhookToken = req.headers["x-webhook-token"] || "";
	if (webhookToken === getWebhookToken()) {
		return next();
	} else {
		return res.status(404).json({
			message: "token is missing or incorrect"
		});
	}
};

/**
 * A middleware to check incoming HTTP Payload exists for update-fake-id webhook
 * @param {*} req express req
 * @param {*} res express res
 * @param {*} next express next
 */
const checkUpdateFakeIdPayload = (req, res, next) => {
	if (!req.body || !req.body.next_fake_id) {
		return res.status(404).json({
			message: "no next_fake_id in body"
		});
	} else {
		next();
	}
};

const webhooksRouter = express.Router();

module.exports = sockets => {
	webhooksRouter.post(
		"/update-fake-id",
		checkWebhookToken,
		checkUpdateFakeIdPayload,
		(req, res) => {
			const nextFakeId = req.body.next_fake_id;
			setImmediate(() => {
				Object.values(sockets).forEach(socket => {
					socket.emit(UPDATE_FAKE_ID, { nextFakeId });
				});
			});
			return res.status(200).json({});
		}
	);
	return webhooksRouter;
};
