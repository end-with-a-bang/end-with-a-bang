require("@babel/register")({
	babelrc: false,
	presets: [
		[
			"@babel/preset-env",
			{
				targets: {
					node: "current"
				}
			}
		]
	]
});

require("./server.js");
