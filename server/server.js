/* eslint-disable no-console */
// https://github.com/sergiodxa/next-socket.io/blob/master/server.js
// https://github.com/zeit/next.js/blob/master/examples/custom-server-express/server.js
const express = require("express");
const { join } = require("path");
const next = require("next");
const bodyParser = require("body-parser");

// socket.io
const http = require("http");
const socketIO = require("socket.io");

// logging
const fs = require("fs");
const morgan = require("morgan");

const webhooksRouter = require("./webhooks");
const uuid = require("uuid/v4");

const { getTweets } = require("../utils/tweets/controller");
const { TWEET_PER_PAGE } = require("../constants/tweets");

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

const logStream = fs.createWriteStream(
	join(__dirname, "..", ".logs", "access.log"),
	{
		flags: "a"
	}
);

const allSockets = {};

app.prepare().then(() => {
	const server = express();

	const httpWrappedServer = http.createServer(server);
	const io = socketIO(httpWrappedServer);

	io.on("connection", socket => {
		const randomId = uuid();
		allSockets[randomId] = socket;
		console.log("a user connected");
		console.log(`total user: ${Object.keys(allSockets).length}`);
		socket.on("disconnect", () => {
			//do some cleanup
			delete allSockets[randomId];
		});
	});

	const logger = morgan(
		// eslint-disable-next-line prettier/prettier
		`${
			process.env.SHORT_SHA
		} :remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"`,
		{
			skip: (req, res) => {
				if (req.url === "/health" && res.statusCode < 400) {
					return true;
				}
				return false;
			},
			stream: logStream
		}
	);

	server.use(logger);

	server.get("/service-worker.js", (req, res) => {
		const filePath = join(__dirname, "..", ".next", "/service-worker.js");
		app.serveStatic(req, res, filePath);
	});

	server.get("/favicon.ico", (req, res) => {
		const filePath = join(__dirname, "..", "static", "/favicon.ico");
		app.serveStatic(req, res, filePath);
	});

	server.get("/api/v1/tweets", (req, res) => {
		let { afterId, count = TWEET_PER_PAGE } = req.query;
		afterId = parseInt(afterId, 10) || 0;
		count = parseInt(count, 10) || 0;
		getTweets({ afterId, count }).then(r => {
			res.json(r);
		});
	});

	server.get("/health", (req, res) => {
		return res.status(204).end();
	});

	server.use("/webhooks", bodyParser.json(), webhooksRouter(allSockets));

	server.get("*", (req, res) => {
		return handle(req, res);
	});
	httpWrappedServer.listen(port, err => {
		if (err) {
			console.log("encounter err when starting express server");
			!!err.message && console.log(`err.message is ${err.message}`);
			throw err;
		}
		console.log(`> Ready on http://localhost:${port}`);
	});
});
