// Lots of the set up is from the next with-jest example
// https://github.com/zeit/next.js/blob/canary/examples/with-jest/jest.setup.js
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });
