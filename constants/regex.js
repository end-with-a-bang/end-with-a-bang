/* eslint no-useless-escape: "off"*/

// TODO:
// My deepest condolences to the victims of the terrible shooting in Douglas County @DCSheriff, and their families. We love our police and law enforcement - God Bless them all! #LESM
// What a year it’s been, and we're just getting started. Together, we are MAKING AMERICA GREAT AGAIN! Happy New Year!! https://t.co/qsMNyN1UJG
// Together, we are MAKING AMERICA GREAT AGAIN! https://t.co/OeyTdFyl1Q https://t.co/wdLQkfdy4m 2 links
// Jobs are kicking in and companies are coming back to the U.S. Unnecessary regulations and high taxes are being dramatically Cut, and it will only get better. MUCH MORE TO COME! // this all caps
// The entire world understands that the good people of Iran want change, and, other than the vast military power of the United States, that Iran’s people are what their leaders fear the most.... https://t.co/W8rKN9B6RT ellispse

// These two regex may be useful
// export const lastBangWithURL = /\b([0-9a-zA-Z]+\!+)(?:\s)*(((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?)*$/;
// export const lastBang = /\b[0-9a-zA-Z]+\?*\!+\?*$/;

export const lastBangWithURLHashtag = /\b([0-9a-zA-Z]+\)*(\!|\?|\u2026|\.{4,})+)(?:\s)*((((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?(?:\s)*)*|((?:\s)*(\#[0-9a-zA-Z]+)(?:\s)*)*|((?:\s)*\@[0-9a-zA-Z]+)*)*$/u;

export const lastAllUppercase = /\b([A-Z\s]+\)*(\!|\?|\u2026|\.{4,})*)(?:\s)*((((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?(?:\s)*)*|((?:\s)*(\#[0-9a-zA-Z]+)(?:\s)*)*|((?:\s)*\@[0-9a-zA-Z]+)*)*$/u;

// example: ..... Happy New Year!!! OR  .....MUCH MORE TO COME!
export const lastCapWord = /(([\,\s]*[A-Z]\w+)+(\!|\?|\u2026|\.{4,})*)(?:\s)*((((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?(?:\s)*)*|((?:\s)*(\#[0-9a-zA-Z]+)(?:\s)*)*|((?:\s)*\@[0-9a-zA-Z]+)*)*$/u;

export const startWithQuote = /^\u201c(.)+\u201d/u;

// useful to strip out hashtag, links etc
export const notSpaceWordDigitAndUsefulPunc = /[^\w\d\s!?\u2026.,]/;
// useful to remove end hashtag or links
export const endHashLink = /((((http[s]?|ftp):\/){1,1}\/{1,1}([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?(?:\s)*)*|((?:\s)*(\#[0-9a-zA-Z]+)(?:\s)*)*|((?:\s)*\@[0-9a-zA-Z]+)*)*$/;
