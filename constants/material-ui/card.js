export const cardStyles = {
	overflow: "visible",
	height: "85vh",
	width: "98%",
	maxWidth: "98%",
	minWidth: "98%",
	minHeight: "80%",
	maxHeight: "100%"
};

export const cardContentStyles = {
	height: "100%"
};

export const endBangCardStyles = {
	overflow: "visible",
	height: "85vh",
	width: "100%",
	maxWidth: "100%",
	minWidth: "100%",
	boxShadow: "none",
	minHeight: "80%",
	maxHeight: "100%"
};

export default cardStyles;
