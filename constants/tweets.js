export const TWEET_PER_PAGE = 10;

// endbang Typography variant based on size

export const endBangVariant = {
	xs: "h2",
	sm: "h2",
	md: "h2",
	lg: "h1",
	xl: "h1"
};
