import React from "react";

// hide retweets
const HideRetweetsContext = React.createContext({
	hideRetweets: true,
	toggleHideRetweets: () => {}
});

export default HideRetweetsContext;
