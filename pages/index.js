import getConfig from "next/config";
import Head from "next/head";

import React, { Fragment } from "react";
import PropTypes from "prop-types";

import { SnackbarProvider } from "notistack";

import Tweets from "../components/Tweets";
import AppBar from "../components/AppBar";
import HideRetweetsContext from "../contexts/HideRetweets";
import { getTweets } from "../utils/tweets/controller";
import { warningRed } from "../constants/material-ui/colors";

const { publicRuntimeConfig } = getConfig();

import { withStyles } from "@material-ui/core/styles";

const styles = {
	error: {
		height: "64px",
		backgroundColor: warningRed,
		left: "0px"
	}
};

class Landing extends React.Component {
	static async getInitialProps() {
		const tweets = (await getTweets({})) || [];
		const afterId = tweets.length ? tweets[tweets.length - 1].fake_id : "";
		return { tweets, afterId };
	}
	static propTypes = {
		tweets: PropTypes.arrayOf(PropTypes.object).isRequired,
		afterId: PropTypes.number,
		classes: PropTypes.object
	};
	static defaultProps = {
		afterId: null
	};
	state = {
		hideRetweets: true,
		initialzed: false
	};
	componentDidMount() {
		let hideRetweets = true;
		try {
			hideRetweets = localStorage.getItem("hideRetweets") !== "false";
		} catch (err) {
			// eslint-disable-next-line no-console
			console.log(
				`error occurs when mounting Landing Page, error is ${err}`
			);
		} finally {
			this.setState({ initialzed: true, hideRetweets });
			localStorage.setItem("hideRetweets", hideRetweets.toString());
		}
	}

	toggleHideRetweets = () => {
		let newHideRetweets = !this.state.hideRetweets;
		this.setState({ hideRetweets: newHideRetweets });
		localStorage.setItem("hideRetweets", newHideRetweets.toString());
	};
	render() {
		const { tweets, afterId, classes } = this.props;
		return (
			<Fragment>
				<Head>
					<title>End With A Bang!</title>
					<meta
						name="SHORT_SHA"
						content={publicRuntimeConfig.shortSHA}
					/>
					<meta
						name="Description"
						content="What is @realDonaldTrump exclaiming about"
					/>
				</Head>
				<SnackbarProvider
					maxSnack={1}
					dense
					iconVariant={{ error: "" }}
					classes={{
						variantError: classes.error
					}}
				>
					<HideRetweetsContext.Provider
						value={{
							hideRetweets: this.state.hideRetweets,
							toggleHideRetweets: this.toggleHideRetweets
						}}
					>
						<AppBar />
						<Tweets tweets={tweets} afterId={afterId} />
					</HideRetweetsContext.Provider>
				</SnackbarProvider>
			</Fragment>
		);
	}
}

export default withStyles(styles)(Landing);
