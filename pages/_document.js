// https://raw.githubusercontent.com/mui-org/material-ui/master/examples/nextjs/pages/_document.js
import React from "react";
import PropTypes from "prop-types";
import Document, { Head, Main, NextScript } from "next/document";
import flush from "styled-jsx/server";

class MyDocument extends Document {
	render() {
		const { pageContext } = this.props;

		return (
			<html lang="en" dir="ltr">
				<Head>
					<meta charSet="utf-8" />
					{/* Use minimum-scale=1 to enable GPU rasterization */}
					<meta
						name="viewport"
						content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
					/>
					{/* PWA primary color */}
					<meta
						rel="preload"
						name="theme-color"
						content={
							pageContext
								? pageContext.theme.palette.primary.main
								: null
						}
					/>
					<link rel="manifest" href="/static/manifest.json" />
					{/* apple devices icon for PWA */}
					<link
						rel="apple-touch-icon"
						href="/static/apple-icon.png"
					/>
					<link
						sizes="152x152"
						rel="apple-touch-icon"
						href="/static/apple-icon-152x152.png"
					/>
					<link
						sizes="180x180"
						rel="apple-touch-icon"
						href="/static/apple-icon-180x180.png"
					/>
					<link
						sizes="167x167"
						rel="apple-touch-icon"
						href="/static/apple-icon-180x180.png"
					/>
					{/* Splash Screen */}
					<link
						href="/static/splashscreens/iphone5_splash.png"
						media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)"
						rel="apple-touch-startup-image"
					/>
					<link
						href="/static/splashscreens/iphone6_splash.png"
						media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)"
						rel="apple-touch-startup-image"
					/>
					<link
						href="/static/splashscreens/iphoneplus_splash.png"
						media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)"
						rel="apple-touch-startup-image"
					/>
					<link
						href="/static/splashscreens/iphonex_splash.png"
						media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)"
						rel="apple-touch-startup-image"
					/>
					<link
						href="/static/splashscreens/iphonexr_splash.png"
						media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)"
						rel="apple-touch-startup-image"
					/>
					<link
						href="/static/splashscreens/iphonexsmax_splash.png"
						media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)"
						rel="apple-touch-startup-image"
					/>
					<link
						href="/static/splashscreens/ipad_splash.png"
						media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)"
						rel="apple-touch-startup-image"
					/>
					<link
						href="/static/splashscreens/ipadpro1_splash.png"
						media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)"
						rel="apple-touch-startup-image"
					/>
					<link
						href="/static/splashscreens/ipadpro3_splash.png"
						media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)"
						rel="apple-touch-startup-image"
					/>
					<link
						href="/static/splashscreens/ipadpro2_splash.png"
						media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)"
						rel="apple-touch-startup-image"
					/>
				</Head>
				<body>
					<Main />
					<NextScript />
					<link
						rel="stylesheet"
						href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
					/>
				</body>
			</html>
		);
	}
}

MyDocument.getInitialProps = ctx => {
	// Resolution order
	//
	// On the server:
	// 1. app.getInitialProps
	// 2. page.getInitialProps
	// 3. document.getInitialProps
	// 4. app.render
	// 5. page.render
	// 6. document.render
	//
	// On the server with error:
	// 1. document.getInitialProps
	// 2. app.render
	// 3. page.render
	// 4. document.render
	//
	// On the client
	// 1. app.getInitialProps
	// 2. page.getInitialProps
	// 3. app.render
	// 4. page.render

	// Render app and page and get the context of the page with collected side effects.
	let pageContext;
	const page = ctx.renderPage(Component => {
		const WrappedComponent = props => {
			pageContext = props.pageContext;
			return <Component {...props} />;
		};

		WrappedComponent.propTypes = {
			pageContext: PropTypes.object.isRequired
		};

		return WrappedComponent;
	});

	let css;
	// It might be undefined, e.g. after an error.
	if (pageContext) {
		css = pageContext.sheetsRegistry.toString();
	}

	return {
		...page,
		pageContext,
		// Styles fragment is rendered after the app and page rendering finish.
		styles: (
			<React.Fragment>
				<style
					id="jss-server-side"
					// eslint-disable-next-line react/no-danger
					dangerouslySetInnerHTML={{ __html: css }}
				/>
				{flush() || null}
			</React.Fragment>
		)
	};
};

export default MyDocument;
