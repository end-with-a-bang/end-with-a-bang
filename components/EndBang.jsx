import React from "react";
import PropTypes from "prop-types";
import withWidth from "@material-ui/core/withWidth";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import { withStyles } from "@material-ui/core/styles";

import Fade from "react-reveal/Fade";

import { endBangVariant } from "../constants/tweets";
import {
	cardContentStyles,
	endBangCardStyles
} from "../constants/material-ui/card";

const EndBang = props => {
	const variant = endBangVariant[props.width] || "h5";
	return (
		<Card className={props.classes.card}>
			<CardContent className={props.classes.cardContent}>
				<div className={props.classes.endBangContainer}>
					<Fade bottom cascade delay={150}>
						<Typography
							variant={variant}
							align="center"
							className={props.classes.endBang}
						>
							{props.endBang}
						</Typography>
					</Fade>
				</div>
			</CardContent>
		</Card>
	);
};

EndBang.propTypes = {
	width: PropTypes.string.isRequired,
	endBang: PropTypes.string.isRequired,
	classes: PropTypes.shape({
		card: PropTypes.string.isRequired,
		cardContent: PropTypes.string.isRequired,
		endBangContainer: PropTypes.string.isRequired,
		endBang: PropTypes.string.isRequired
	}).isRequired
};

const endBangStyles = theme => ({
	card: {
		...endBangCardStyles,
		width: "100%",
		minHeight: "50%",
		[theme.breakpoints.up("md")]: {
			minHeight: "80%"
		}
	},
	cardContent: {
		...cardContentStyles,
		display: "flex",
		flexDirection: "column",
		justifyContent: "center"
	},
	endBangContainer: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		marginTop: "0.5em",
		position: "sticky",
		// somehow get the values?
		top: "65px",
		backgroundColor: "white",
		zIndex: 10,
		fontWeight: "400"
	},
	endBang: {
		fontWeight: "400"
	}
});

const WithStyleEndBang = withStyles(endBangStyles)(EndBang);

export default withWidth({ resizeInterval: 300 })(WithStyleEndBang);
