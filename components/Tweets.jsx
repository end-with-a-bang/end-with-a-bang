import React, { Fragment, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useSnackbar } from "notistack";
import { withStyles } from "@material-ui/core/styles";
import Head from "next/head";
import Waypoint from "react-waypoint";
import LinearProgress from "@material-ui/core/LinearProgress";
import RefreshIcon from "@material-ui/icons/Refresh";
import debounce from 'lodash.debounce'; // eslint-disable-line
import fetch from "isomorphic-fetch";

import io from "socket.io-client";

import HideRetweetsContext from "../contexts/HideRetweets";
import Tweet from "./Tweet";
import { UPDATE_FAKE_ID } from "../constants/event-constants";
import { scrollToTop } from "../utils/global/scrollToTop";

const style = () => {
	return {
		refreshIcon: {
			cursor: "pointer"
		}
	};
};

function Tweets(props) {
	const [tweets, setTweets] = useState(props.tweets || []);
	const [tweetInView, setTweetInView] = useState(null);
	const [headTitle, setHeadTitle] = useState("End With A Bang!");
	const [loading, setLoading] = useState(false);
	const [afterId, setAfterId] = useState(props.afterId);
	const { enqueueSnackbar } = useSnackbar();

	const _onTweetScrollIntoView = ({ text, tweetId, endBang }) => {
		setTweetInView({ text, tweetId, endBang });
		setHeadTitle(endBang || "End With A Bang!");
	};
	const onTweetScrollIntoView = debounce(_onTweetScrollIntoView, 500);
	const loadMore = async () => {
		if (loading) {
			return;
		}
		setLoading(true);
		const url = "/api/v1/tweets" + (afterId ? "?afterId=" + afterId : "");
		const newTweets = await fetch(url).then(r => r.json());
		setTweets([...tweets, ...newTweets]);
		setLoading(false);
		setAfterId(newTweets[newTweets.length - 1].fake_id);
	};

	useEffect(() => {
		const socket = io();
		socket.on(UPDATE_FAKE_ID, () => {
			enqueueSnackbar("New tweets! Refresh to see NEW TWEETS!!", {
				variant: "error",
				persist: true,
				anchorOrigin: {
					vertical: "top",
					horizontal: "center"
				},
				action: (
					<RefreshIcon
						style={{ paddingLeft: 0 }}
						className={props.classes.refreshIcon}
						onClick={() => {
							scrollToTop(5);
							location.reload(true);
						}}
					/>
				)
			});
		});
		return socket.close;
	}, []);

	return (
		<Fragment>
			{!!tweetInView && (
				<Head>
					<title>{headTitle}</title>
				</Head>
			)}
			<div className="tweets-list">
				<HideRetweetsContext.Consumer>
					{({ hideRetweets }) => {
						const filtedTweets = tweets.filter(t =>
							hideRetweets ? !t.is_retweet : true
						);
						return (
							<Fragment>
								{filtedTweets.slice(0, -3).map(t => (
									<Tweet
										onTweetScrollIntoView={
											onTweetScrollIntoView
										}
										tweet={t}
										key={t.id_str}
									/>
								))}
								<Waypoint onEnter={loadMore} />
								{filtedTweets.slice(-3).map(t => (
									<Tweet
										onTweetScrollIntoView={
											onTweetScrollIntoView
										}
										tweet={t}
										key={t.id_str}
									/>
								))}
							</Fragment>
						);
					}}
				</HideRetweetsContext.Consumer>
				<span
					style={{
						position: "fixed",
						left: "50%",
						bottom: "15px"
					}}
				>
					<Waypoint onEnter={loadMore} />
				</span>
			</div>
			{loading && <LinearProgress />}
		</Fragment>
	);
}

Tweets.propTypes = {
	tweets: PropTypes.arrayOf(PropTypes.object).isRequired,
	classes: PropTypes.shape({
		refreshIcon: PropTypes.string
	}).isRequired,
	afterId: PropTypes.number
};

export default withStyles(style)(Tweets);
