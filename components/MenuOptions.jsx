import React from "react";
import PropTypes from "prop-types";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Switch from "@material-ui/core/Switch";

import HideRetweetsContext from "../contexts/HideRetweets";

const MenuOptions = ({ anchorEl, setAnchorEl, isMenuOpen, setMenuOpen }) => {
	return (
		<Menu
			id="long-menu"
			anchorEl={anchorEl}
			open={isMenuOpen}
			onClose={e => {
				e.stopPropagation();
				setMenuOpen(false);
				setAnchorEl(null);
			}}
			PaperProps={{
				style: {
					maxHeight: 48 * 4.5,
					width: 200
				}
			}}
		>
			<MenuItem disableRipple>
				Hide Retweets
				<HideRetweetsContext.Consumer>
					{({ hideRetweets, toggleHideRetweets }) => (
						<Switch
							checked={hideRetweets}
							value={hideRetweets}
							onChange={toggleHideRetweets}
							onClick={e => {
								e.stopPropagation();
							}}
						/>
					)}
				</HideRetweetsContext.Consumer>
			</MenuItem>
		</Menu>
	);
};

MenuOptions.propTypes = {
	anchorEl: PropTypes.any,
	setAnchorEl: PropTypes.func.isRequired,
	isMenuOpen: PropTypes.bool.isRequired,
	setMenuOpen: PropTypes.func.isRequired
};

export default MenuOptions;
