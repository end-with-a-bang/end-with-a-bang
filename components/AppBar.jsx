import React, { useState } from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";

import MenuOptions from "./MenuOptions";
import { scrollToTop } from "../utils/global/scrollToTop";

import getConfig from "next/config";
const { publicRuntimeConfig } = getConfig();

const styles = theme => {
	return {
		root: {
			marginBottom: theme.spacing.unit * 0.5,
			cursor: "pointer",
			justifyContent: "space-between",
			display: "flex",
			flexDirection: "row"
		},
		grow: {
			flexGrow: 1
		},
		toolbar: {
			justifyContent: "center"
		},
		iconBtn: {
			position: "relative",
			float: "right"
		},
		title: {
			display: "none",
			paddingTop: theme.spacing.unit,
			paddingBottom: theme.spacing.unit,
			marginRight: theme.spacing.unit * 2,
			textAlign: "center",
			color: "#150163",
			[theme.breakpoints.up("xs")]: {
				display: "block"
			}
		}
	};
};

export const EndWithABangAppBar = ({ classes }) => {
	const [isMenuOpen, setMenuOpen] = useState(false);
	const [anchorEl, setAnchorEl] = useState(null);
	return (
		<AppBar
			position="sticky"
			className={classes.root}
			onClick={scrollToTop}
		>
			{" "}
			<Toolbar />
			<Toolbar className={classes.toolbar}>
				<Typography className={classes.title} variant="h6" noWrap>
					End With A Bang!
				</Typography>
				<Avatar
					alt="Trump Picture"
					src={`${publicRuntimeConfig.staticFolder}/apple-icon.png`}
				/>
			</Toolbar>
			<Toolbar>
				<IconButton
					className={classes.iconBtn}
					aria-label="More"
					aria-owns={isMenuOpen ? "long-menu" : undefined}
					aria-haspopup="true"
					onClick={e => {
						e.stopPropagation();
						setMenuOpen(!isMenuOpen);
						setAnchorEl(e.currentTarget);
					}}
				>
					<MoreVertIcon />
				</IconButton>
				{isMenuOpen && (
					<MenuOptions
						isMenuOpen={isMenuOpen}
						setMenuOpen={setMenuOpen}
						anchorEl={anchorEl}
						setAnchorEl={setAnchorEl}
					/>
				)}
			</Toolbar>
		</AppBar>
	);
};

EndWithABangAppBar.propTypes = { classes: PropTypes.object.isRequired };

export default withStyles(styles)(EndWithABangAppBar);
