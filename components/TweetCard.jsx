import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import { withStyles } from "@material-ui/core/styles";
import Fade from "react-reveal/Fade";

import EndBang from "./EndBang";
import { cardContentStyles, cardStyles } from "../constants/material-ui/card";

const TweetCardStyles = () => {
	return {
		root: {
			...cardStyles,
			boxShadow:
				"0px 2px 5px -3px rgba(63, 140, 228, 0.2), 3px 5px 7px 1px rgba(63, 140, 228, 0.05), 2px 3px 7px 2px rgba(63, 140, 228, 0.2)"
		},
		cardContent: {
			...cardContentStyles
		},
		cardContentDiv: {
			height: "100%",
			display: "flex",
			flexDirection: "column",
			justifyContent: "space-around",
			wordWrap: "break-word"
		},
		tweetContent: {
			fontSize: "1rem",
			marginBottom: "0.5rem"
		}
	};
};

function TweetCard(props) {
	const {
		endBang,
		lastCapWord,
		lastHash,
		lastURL,
		// isRetweet,
		classes
	} = props;
	return (
		<Card raised className={classes.root}>
			<CardContent className={classes.cardContent}>
				<Fade bottom cascade delay={200}>
					<div className={classes.cardContentDiv}>
						<EndBang endBang={endBang} />
						{lastCapWord && (
							<Typography
								variant="h3"
								align="center"
								gutterBottom
							>
								{lastCapWord}
							</Typography>
						)}
						{lastHash && (
							<Typography
								variant="h6"
								align="center"
								gutterBottom
							>
								{lastHash}
							</Typography>
						)}
						{lastURL && (
							<Typography
								variant="h6"
								align="center"
								gutterBottom
							>
								{lastURL}
							</Typography>
						)}
						<Typography
							className={classes.tweetContent}
							paragraph
							dangerouslySetInnerHTML={{
								__html: props.tweet.text
							}}
						/>
					</div>
				</Fade>
			</CardContent>
		</Card>
	);
}

TweetCard.propTypes = {
	endBang: PropTypes.string.isRequired,
	lastCapWord: PropTypes.string,
	lastHash: PropTypes.arrayOf(PropTypes.string).isRequired,
	lastURL: PropTypes.arrayOf(PropTypes.string).isRequired,
	isRetweet: PropTypes.bool.isRequired,
	tweet: PropTypes.shape({
		text: PropTypes.string.isRequired
	}).isRequired,
	classes: PropTypes.shape({
		root: PropTypes.string.isRequired
	}).isRequired
};

export default withStyles(TweetCardStyles)(TweetCard);
