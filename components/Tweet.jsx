import React, { useState } from "react";
import Waypoint from "react-waypoint";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

import TweetCard from "./TweetCard";

import processTweet, { isRetweet } from "../utils/tweets/processTweets";

const TweetStyles = () => {
	return {
		tweetCardContainer: {
			minHeight: "150%",
			// minHeight: "200%",
			height: "150%",
			display: "flex",
			justifyContent: "center",
			marginBottom: "25%"
		}
	};
};
function Tweet(props) {
	const [state] = useState(() => {
		const {
			punctuation,
			firstQuote,
			lastCapWord,
			lastHash,
			lastURL,
			fallback
		} = processTweet(props.tweet.text);
		const endBang = punctuation || lastCapWord || firstQuote || fallback;
		return {
			punctuation,
			firstQuote,
			lastCapWord,
			lastHash,
			lastURL,
			fallback,
			endBang,
			isRetweet: isRetweet(props.tweet)
		};
	});
	const onEnter = () => {
		props.onTweetScrollIntoView({
			text: props.tweet.text,
			tweetId: props.tweet.id_str,
			endBang: state.endBang
		});
	};

	const { classes } = props;
	return (
		<Waypoint onEnter={onEnter}>
			<div className={classes.tweetCardContainer}>
				<TweetCard {...state} tweet={props.tweet} />
			</div>
		</Waypoint>
	);
}

Tweet.propTypes = {
	tweet: PropTypes.shape({
		text: PropTypes.string.isRequired,
		id_str: PropTypes.string.isRequired
	}).isRequired,
	onTweetScrollIntoView: PropTypes.func.isRequired,
	classes: PropTypes.object.isRequired
};
export default withStyles(TweetStyles)(Tweet);
